# Contributor: Milan P. Stanić <mps@arvanta.net>
# Maintainer: Milan P. Stanić <mps@arvanta.net>
pkgname=zig
pkgver=0.5.0_git20200329
_gitcommit=86795c03f918dbcc0fc06811cc7c496d275deae1
pkgrel=1
pkgdesc="general-purpose programming language designed for robustness, optimality, and maintainability"
url="https://ziglang.org/"
arch="x86_64"
license="MIT"
provides="zig-dev=$pkgver-r$pkgrel"
options="!check" # works, but fail at the end, need to be fixed
makedepends="cmake clang-dev clang-libs llvm10-libs llvm-dev lld-dev libstdc++
	zlib-static libxml2-dev llvm10-static clang-static lld-static"
source="$pkgname-$pkgver.tar.gz::https://github.com/ziglang/zig/archive/$_gitcommit.tar.gz"

builddir="$srcdir"/zig-$_gitcommit

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DZIG_FORCE_EXTERNAL_LLD=ON \
		-DZIG_SKIP_INSTALL_LIB_FILES=ON \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	./zig build test
}

package() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DZIG_FORCE_EXTERNAL_LLD=ON \
		-DZIG_SKIP_INSTALL_LIB_FILES=OFF \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make DESTDIR="$pkgdir" install
}

sha512sums="f8d0f43eae4cae0b1def537f12514830764b354592ce6e5292e6fb73ff56fc8e07ba59fdf8e7995d48a14c8682e70ce28c271cc395e2f99f0f2415edb9944434  zig-0.5.0_git20200329.tar.gz"
